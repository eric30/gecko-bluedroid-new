/* -*- Mode: c++; c-basic-offset: 2; indent-tabs-mode: nil; tab-width: 40 -*- */
/* vim: set ts=2 et sw=2 tw=80: */
/*
** Copyright 2006, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/
#include "base/basictypes.h"
#include "BluetoothHfpManager.h"
#include "BluetoothOppManager.h"
#include "BluetoothReplyRunnable.h"
#include "BluetoothUnixSocketConnector.h"
#include "BluetoothUtils.h"
#include "BluetoothUuid.h"
#include "BluetoothDroidService.h"
#include <cstdio>

#include "pratom.h"
#include "nsAutoPtr.h"
#include "nsThreadUtils.h"
#include "nsDebug.h"
#include "nsDataHashtable.h"
#include "mozilla/Hal.h"
#include "mozilla/ipc/UnixSocket.h"
#include "mozilla/Util.h"
#include "mozilla/NullPtr.h"
#include "mozilla/dom/bluetooth/BluetoothTypes.h"
#if defined(MOZ_WIDGET_GONK)
#include "cutils/properties.h"
#endif

using namespace mozilla;
using namespace mozilla::ipc;
USING_BLUETOOTH_NAMESPACE


bool
BluetoothDroidService::IsReady()
{
  return true;
}

nsresult
BluetoothDroidService::StartInternal()
{

  return NS_OK;
}


nsresult
BluetoothDroidService::StopInternal()
{
  return NS_OK;
}

bool
BluetoothDroidService::IsEnabledInternal()
{
  return false;
}

class DefaultAdapterPropertiesRunnable : public nsRunnable
{
public:
  DefaultAdapterPropertiesRunnable(BluetoothReplyRunnable* aRunnable)
    : mRunnable(dont_AddRef(aRunnable))
  {
  }

  nsresult
  Run()
  {
    MOZ_ASSERT(!NS_IsMainThread());

    BluetoothValue v;
    nsAutoString replyError;
#if 0
    if (NS_FAILED(GetDefaultAdapterPath(v, replyError))) {
      DispatchBluetoothReply(mRunnable, v, replyError);
      return NS_ERROR_FAILURE;
    }

    DBusError err;
    dbus_error_init(&err);
    nsString objectPath = v.get_nsString();

#endif
    v = InfallibleTArray<BluetoothNamedValue>();
#if 0
    if (!GetPropertiesInternal(objectPath, DBUS_ADAPTER_IFACE, v)) {
      NS_WARNING("Getting properties failed!");
      return NS_ERROR_FAILURE;
    }
    // We have to manually attach the path to the rest of the elements
    v.get_ArrayOfBluetoothNamedValue().AppendElement(
      BluetoothNamedValue(NS_LITERAL_STRING("Path"), objectPath));
#endif
    DispatchBluetoothReply(mRunnable, v, EmptyString());

    return NS_OK;
  }

private:
  nsRefPtr<BluetoothReplyRunnable> mRunnable;
};

nsresult
BluetoothDroidService::GetDefaultAdapterPathInternal(
                                              BluetoothReplyRunnable* aRunnable)
{
#if 0
  //shawn
  BT_LOGD("BluetoothDroidService::GetDefaultAdapterPathInternal ");
  NS_ASSERTION(NS_IsMainThread(), "Must be called from main thread!");

  if (!IsReady()) {
    BluetoothValue v;
    nsAutoString errorStr;
    errorStr.AssignLiteral("Bluetooth service is not ready yet!");
    DispatchBluetoothReply(aRunnable, v, errorStr);
    return NS_OK;
  }

  nsRefPtr<BluetoothReplyRunnable> runnable = aRunnable;
  nsRefPtr<nsRunnable> func(new DefaultAdapterPropertiesRunnable(runnable));
  if (NS_FAILED(mBluetoothCommandThread->Dispatch(func, NS_DISPATCH_NORMAL))) {
    NS_WARNING("Cannot dispatch firmware loading task!");
    return NS_ERROR_FAILURE;
  }

  runnable.forget();
#endif
  return NS_OK;

}

nsresult
BluetoothDroidService::StopDiscoveryInternal(BluetoothReplyRunnable* aRunnable)
{
  return NS_OK;
}

nsresult
BluetoothDroidService::StartDiscoveryInternal(BluetoothReplyRunnable* aRunnable)
{
  //return SendDiscoveryMessage("StartDiscovery", aRunnable);
  BT_LOGD("++++++++++++++++ %s   +++++++++++++++", __FUNCTION__);
  return NS_OK;
}
#if 0
class BluetoothDevicePropertiesRunnable : public nsRunnable
{
public:
  BluetoothDevicePropertiesRunnable(const BluetoothSignal& aSignal) :
    mSignal(aSignal)
  {
    MOZ_ASSERT(NS_IsMainThread());
  }

  NS_IMETHOD Run()
  {
    MOZ_ASSERT(!NS_IsMainThread());

    const InfallibleTArray<BluetoothNamedValue>& arr =
      mSignal.value().get_ArrayOfBluetoothNamedValue();
    nsString devicePath = arr[0].value().get_nsString();

    BluetoothValue prop;
    bool rv = GetPropertiesInternal(devicePath, DBUS_DEVICE_IFACE, prop);
    NS_ENSURE_TRUE(rv, NS_ERROR_FAILURE);

    // Return original dbus message parameters and also device name
    // for agent events "RequestConfirmation", "RequestPinCode",
    // and "RequestPasskey"
    InfallibleTArray<BluetoothNamedValue>& parameters =
      mSignal.value().get_ArrayOfBluetoothNamedValue();

    // Replace object path with device address
    nsString address = GetAddressFromObjectPath(devicePath);
    parameters[0].name().AssignLiteral("address");
    parameters[0].value() = address;

    InfallibleTArray<BluetoothNamedValue>& properties =
      prop.get_ArrayOfBluetoothNamedValue();
    uint8_t i;
    for (i = 0; i < properties.Length(); i++) {
      // Append device name
      if (properties[i].name().EqualsLiteral("Name")) {
        properties[i].name().AssignLiteral("name");
        parameters.AppendElement(properties[i]);
        break;
      }
    }
    MOZ_ASSERT(i != properties.Length(), "failed to get device name");

    nsRefPtr<DistributeBluetoothSignalTask> t =
      new DistributeBluetoothSignalTask(mSignal);
    if (NS_FAILED(NS_DispatchToMainThread(t))) {
      NS_WARNING("Failed to dispatch to main thread!");
      return NS_ERROR_FAILURE;
    }

    return NS_OK;
  }

private:
  BluetoothSignal mSignal;
};
#endif

nsresult
BluetoothDroidService::GetDevicePropertiesInternal(const BluetoothSignal& aSignal)
{
  MOZ_ASSERT(NS_IsMainThread());

  return NS_OK;
}

nsresult
BluetoothDroidService::GetConnectedDevicePropertiesInternal(uint16_t aProfileId,
                                              BluetoothReplyRunnable* aRunnable)
{
  return NS_OK;
}

nsresult
BluetoothDroidService::GetPairedDevicePropertiesInternal(
                                     const nsTArray<nsString>& aDeviceAddresses,
                                     BluetoothReplyRunnable* aRunnable)
{
  return NS_OK;
}

nsresult
BluetoothDroidService::SetProperty(BluetoothObjectType aType,
                                  const BluetoothNamedValue& aValue,
                                  BluetoothReplyRunnable* aRunnable)
{
  return NS_OK;
}

bool
BluetoothDroidService::GetDevicePath(const nsAString& aAdapterPath,
                                    const nsAString& aDeviceAddress,
                                    nsAString& aDevicePath)
{
  return true;
}
#if 0
int
GetDeviceServiceChannel(const nsAString& aObjectPath,
                        const nsAString& aPattern,
                        int aAttributeId)
{
  // This is a blocking call, should not be run on main thread.
  MOZ_ASSERT(!NS_IsMainThread());

#ifdef MOZ_WIDGET_GONK
  // GetServiceAttributeValue only exists in android's bluez dbus binding
  // implementation
  nsCString tempPattern = NS_ConvertUTF16toUTF8(aPattern);
  const char* pattern = tempPattern.get();

  DBusMessage *reply =
    dbus_func_args(gThreadConnection->GetConnection(),
                   NS_ConvertUTF16toUTF8(aObjectPath).get(),
                   DBUS_DEVICE_IFACE, "GetServiceAttributeValue",
                   DBUS_TYPE_STRING, &pattern,
                   DBUS_TYPE_UINT16, &aAttributeId,
                   DBUS_TYPE_INVALID);

  return reply ? dbus_returns_int32(reply) : -1;
#else
  // FIXME/Bug 793977 qdot: Just return something for desktop, until we have a
  // parser for the GetServiceAttributes xml block
  return 1;
#endif
}
#endif
// static
bool
BluetoothDroidService::RemoveReservedServicesInternal(
                                      const nsTArray<uint32_t>& aServiceHandles)
{
  return true;
}

nsresult
BluetoothDroidService::CreatePairedDeviceInternal(
                                              const nsAString& aDeviceAddress,
                                              int aTimeout,
                                              BluetoothReplyRunnable* aRunnable)
{
  return NS_OK;
}

nsresult
BluetoothDroidService::RemoveDeviceInternal(const nsAString& aDeviceAddress,
                                           BluetoothReplyRunnable* aRunnable)
{
  return NS_OK;
}

bool
BluetoothDroidService::SetPinCodeInternal(const nsAString& aDeviceAddress,
                                         const nsAString& aPinCode,
                                         BluetoothReplyRunnable* aRunnable)
{
  return true;
}

bool
BluetoothDroidService::SetPasskeyInternal(const nsAString& aDeviceAddress,
                                         uint32_t aPasskey,
                                         BluetoothReplyRunnable* aRunnable)
{
  return true;
}

bool
BluetoothDroidService::SetPairingConfirmationInternal(
                                              const nsAString& aDeviceAddress,
                                              bool aConfirm,
                                              BluetoothReplyRunnable* aRunnable)
{
  return true;
}

bool
BluetoothDroidService::SetAuthorizationInternal(
                                              const nsAString& aDeviceAddress,
                                              bool aAllow,
                                              BluetoothReplyRunnable* aRunnable)
{
  return true;
}

nsresult
BluetoothDroidService::PrepareAdapterInternal()
{
  return NS_OK;
}

void
BluetoothDroidService::Connect(const nsAString& aDeviceAddress,
    uint32_t aCod,
    uint16_t aServiceUuid,
    BluetoothReplyRunnable* aRunnable)
{
}

void
BluetoothDroidService::Disconnect(const nsAString& aDeviceAddress,
    uint16_t aServiceUuid,
    BluetoothReplyRunnable* aRunnable)
{
}

bool
BluetoothDroidService::IsConnected(const uint16_t aProfileId)
{
 return false;
}

#if 0
class ConnectBluetoothSocketRunnable : public nsRunnable
{
public:
  ConnectBluetoothSocketRunnable(BluetoothReplyRunnable* aRunnable,
                                 UnixSocketConsumer* aConsumer,
                                 const nsAString& aObjectPath,
                                 const nsAString& aServiceUUID,
                                 BluetoothSocketType aType,
                                 bool aAuth,
                                 bool aEncrypt,
                                 int aChannel)
    : mRunnable(dont_AddRef(aRunnable))
    , mConsumer(aConsumer)
    , mObjectPath(aObjectPath)
    , mServiceUUID(aServiceUUID)
    , mType(aType)
    , mAuth(aAuth)
    , mEncrypt(aEncrypt)
    , mChannel(aChannel)
  {
  }

  nsresult
  Run()
  {
    MOZ_ASSERT(NS_IsMainThread());

    nsString address = GetAddressFromObjectPath(mObjectPath);
    BluetoothValue v;
    nsAutoString replyError;
    BluetoothUnixSocketConnector* c =
      new BluetoothUnixSocketConnector(mType, mChannel, mAuth, mEncrypt);
    if (!mConsumer->ConnectSocket(c, NS_ConvertUTF16toUTF8(address).get())) {
      replyError.AssignLiteral("SocketConnectionError");
      DispatchBluetoothReply(mRunnable, v, replyError);
      return NS_ERROR_FAILURE;
    }
    return NS_OK;
  }

private:
  nsRefPtr<BluetoothReplyRunnable> mRunnable;
  nsRefPtr<UnixSocketConsumer> mConsumer;
  nsString mObjectPath;
  nsString mServiceUUID;
  BluetoothSocketType mType;
  bool mAuth;
  bool mEncrypt;
  int mChannel;
};

class OnUpdateSdpRecordsRunnable : public nsRunnable
{
public:
  OnUpdateSdpRecordsRunnable(const nsAString& aObjectPath,
                             BluetoothProfileManagerBase* aManager)
    : mManager(aManager)
  {
    MOZ_ASSERT(!aObjectPath.IsEmpty());
    MOZ_ASSERT(aManager);

    mDeviceAddress = GetAddressFromObjectPath(aObjectPath);
  }

  nsresult
  Run()
  {
    MOZ_ASSERT(NS_IsMainThread());

    mManager->OnUpdateSdpRecords(mDeviceAddress);

    return NS_OK;
  }

private:
  nsString mDeviceAddress;
  BluetoothProfileManagerBase* mManager;
};

class OnGetServiceChannelRunnable : public nsRunnable
{
public:
  OnGetServiceChannelRunnable(const nsAString& aObjectPath,
                              const nsAString& aServiceUuid,
                              int aChannel,
                              BluetoothProfileManagerBase* aManager)
    : mServiceUuid(aServiceUuid),
      mChannel(aChannel),
      mManager(aManager)
  {
    MOZ_ASSERT(!aObjectPath.IsEmpty());
    MOZ_ASSERT(!aServiceUuid.IsEmpty());
    MOZ_ASSERT(aManager);

    mDeviceAddress = GetAddressFromObjectPath(aObjectPath);
  }

  nsresult
  Run()
  {
    MOZ_ASSERT(NS_IsMainThread());

    mManager->OnGetServiceChannel(mDeviceAddress, mServiceUuid, mChannel);

    return NS_OK;
  }

private:
  nsString mDeviceAddress;
  nsString mServiceUuid;
  int mChannel;
  BluetoothProfileManagerBase* mManager;
};

class GetServiceChannelRunnable : public nsRunnable
{
public:
  GetServiceChannelRunnable(const nsAString& aObjectPath,
                            const nsAString& aServiceUuid,
                            BluetoothProfileManagerBase* aManager)
    : mObjectPath(aObjectPath),
      mServiceUuid(aServiceUuid),
      mManager(aManager)
  {
    MOZ_ASSERT(!aObjectPath.IsEmpty());
    MOZ_ASSERT(!aServiceUuid.IsEmpty());
    MOZ_ASSERT(aManager);
  }

  nsresult
  Run()
  {
    MOZ_ASSERT(!NS_IsMainThread());

    int channel = GetDeviceServiceChannel(mObjectPath, mServiceUuid, 0x0004);
    nsRefPtr<nsRunnable> r(new OnGetServiceChannelRunnable(mObjectPath,
                                                           mServiceUuid,
                                                           channel,
                                                           mManager));
    if (NS_FAILED(NS_DispatchToMainThread(r))) {
      return NS_ERROR_FAILURE;
    }

    return NS_OK;
  }

private:
  nsString mObjectPath;
  nsString mServiceUuid;
  BluetoothProfileManagerBase* mManager;
};
#endif
nsresult
BluetoothDroidService::GetServiceChannel(const nsAString& aDeviceAddress,
                                        const nsAString& aServiceUuid,
                                        BluetoothProfileManagerBase* aManager)
{

  return NS_OK;
}

static void
DiscoverServicesCallback(DBusMessage* aMsg, void* aData)
{
}

bool
BluetoothDroidService::UpdateSdpRecords(const nsAString& aDeviceAddress,
                                       BluetoothProfileManagerBase* aManager)
{
  return true;
}

nsresult
BluetoothDroidService::GetScoSocket(const nsAString& aAddress,
                                   bool aAuth,
                                   bool aEncrypt,
                                   mozilla::ipc::UnixSocketConsumer* aConsumer)
{
  return NS_OK;
}

void
BluetoothDroidService::SendFile(const nsAString& aDeviceAddress,
                               BlobParent* aBlobParent,
                               BlobChild* aBlobChild,
                               BluetoothReplyRunnable* aRunnable)
{
}

void
BluetoothDroidService::StopSendingFile(const nsAString& aDeviceAddress,
                                      BluetoothReplyRunnable* aRunnable)
{
}

void
BluetoothDroidService::ConfirmReceivingFile(const nsAString& aDeviceAddress,
                                           bool aConfirm,
                                           BluetoothReplyRunnable* aRunnable)
{
}

void
BluetoothDroidService::ConnectSco(BluetoothReplyRunnable* aRunnable)
{
}

void
BluetoothDroidService::DisconnectSco(BluetoothReplyRunnable* aRunnable)
{
}

void
BluetoothDroidService::IsScoConnected(BluetoothReplyRunnable* aRunnable)
{
}

void
BluetoothDroidService::SendMetaData(const nsAString& aTitle,
    const nsAString& aArtist,
    const nsAString& aAlbum,
    int64_t aMediaNumber,
    int64_t aTotalMediaCount,
    int64_t aDuration,
    BluetoothReplyRunnable* aRunnable)
{
}

void
BluetoothDroidService::SendPlayStatus(int64_t aDuration,
    int64_t aPosition,
    const nsAString& aPlayStatus,
    BluetoothReplyRunnable* aRunnable)
{
}

void
BluetoothDroidService::UpdatePlayStatus(uint32_t aDuration,
    uint32_t aPosition,
    ControlPlayStatus aPlayStatus)
{
}

nsresult
BluetoothDroidService::SendSinkMessage(const nsAString& aDeviceAddresses,
    const nsAString& aMessage)
{
  return NS_OK;
}

nsresult
BluetoothDroidService::SendInputMessage(const nsAString& aDeviceAddresses,
                  const nsAString& aMessage)
{
  return NS_OK;
}

